package org.example;

public class Main {
    public static void main(String[] args) {

        ParkeerBoete parkeerBoete = new ParkeerBoete("Jan Smit", "04-PSZ-8", 2);
        parkeerBoete.bepaalBedrag();
        System.out.println(parkeerBoete);


        SnelheidsBoete snelheidsBoete = new SnelheidsBoete("Jan Smit", "04-PSZ-8", 102, 80);
        snelheidsBoete.bepaalBedrag();
        System.out.println(snelheidsBoete);
    }
}