package org.example;

public class VerkeersBoete {

    private String naamDader;
    private String kentekenAuto;
    private double bedrag;

    public VerkeersBoete(String naamDader, String kentekenAuto) {
        this.naamDader = naamDader;
        this.kentekenAuto = kentekenAuto;
    }

    private VerkeersBoete(String naamDader, String kentekenAuto, double bedrag) {
        this.naamDader = naamDader;
        this.kentekenAuto = kentekenAuto;
        this.bedrag = bedrag;
    }

    public String getKentekenAuto() {
        return kentekenAuto;
    }

    public void setKentekenAuto(String kentekenAuto) {
        this.kentekenAuto = kentekenAuto;
    }

    public String getNaamDader() {
        return naamDader;
    }

    public void setNaamDader(String naamDader) {
        this.naamDader = naamDader;
    }

    public void setBedrag(double bedrag) {
        this.bedrag = bedrag;
    }

    public double getBedrag() {
        return bedrag;
    }

    public String toString() {
        return naamDader + " met kenteken " + kentekenAuto + "heeft als boete: " + bedrag;
    }
}
