package org.example;

public class ParkeerBoete extends VerkeersBoete{

    private int zone;

    public ParkeerBoete(String naamDader, String kentekenAuto, int zone) {
        super(naamDader, kentekenAuto);
        this.zone = zone;
    }

    public int getZone() {
        return zone;
    }

    public void setZone(int zone) {
        this.zone = zone;
    }

    public void bepaalBedrag() {
        if(zone == 1) {
            setBedrag(60);
        } else if (zone ==2) {
            setBedrag(100);
        } else {
            setBedrag(180);
        }

    }

    public String toString() {
        return "Parkeerboete voor " + getNaamDader() + ", kenteken " + getKentekenAuto() + ", " + getBedrag() + " euro in zone " + zone;
    }
}
