package org.example;

public class SnelheidsBoete extends VerkeersBoete {

    private int gemetenSnelheid;
    private int maximumSnelheid;

    public SnelheidsBoete(String naamDader, String kentekenAuto, int snelheid, int maximum) {
        super(naamDader, kentekenAuto);
        this.gemetenSnelheid = snelheid;
        this.maximumSnelheid = maximum;
    }


    public int getGemetenSnelheid() {
        return gemetenSnelheid;
    }

    public void setGemetenSnelheid(int gemetenSnelheid) {
        this.gemetenSnelheid = gemetenSnelheid;
    }

    public int getMaximumSnelheid() {
        return maximumSnelheid;
    }

    public void setMaximumSnelheid(int maximumSnelheid) {
        this.maximumSnelheid = maximumSnelheid;
    }

    public void bepaalBedrag() {
        int overtreding = gemetenSnelheid - maximumSnelheid;

        if(overtreding <= 10) {
            setBedrag(10 * overtreding);
        } else if(overtreding <= 30) {
            setBedrag(15 * overtreding);
        } else {
            setBedrag(Math.min(20 * overtreding , 1000));
        }
    }

    public String toString() {
        return "Parkeerboete voor " + getNaamDader() + ", kenteken " + getKentekenAuto() + ", " + getBedrag() + " euro voor " + gemetenSnelheid + " km/u waar " + maximumSnelheid + " km/u mag";
    }
}
