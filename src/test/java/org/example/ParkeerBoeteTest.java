package org.example;

import org.junit.jupiter.api.Assertions;

class ParkeerBoeteTest {

    @org.junit.jupiter.api.Test
    void bepaalBedrag() {
        ParkeerBoete boete = new ParkeerBoete("Jan Smit", "04-PSZ-8", 2);
        boete.bepaalBedrag();

        Assertions.assertEquals(10, boete.getBedrag());
    }
}